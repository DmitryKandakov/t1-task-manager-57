package ru.t1.dkandakov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkandakov.tm.dto.request.user.UserUpdateProfileRequest;
import ru.t1.dkandakov.tm.enumerated.Role;
import ru.t1.dkandakov.tm.event.ConsoleEvent;
import ru.t1.dkandakov.tm.util.TerminalUtil;

@Component
public final class UserUpdateProfileListener extends AbstractUserListener {

    @NotNull
    private final String NAME = "update-user-profile";

    @NotNull
    private final String DESCRIPTION = "update user's profile";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@userUpdateProfileListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[UPDATE USER'S PROFILE]");
        System.out.println("FIRST NAME: ");
        @Nullable final String firstName = TerminalUtil.nextLine();
        System.out.println("LAST NAME: ");
        @Nullable final String lastName = TerminalUtil.nextLine();
        System.out.println("MIDDLE NAME: ");
        @Nullable final String middleName = TerminalUtil.nextLine();
        getUserEndpoint().updateUserProfile(new UserUpdateProfileRequest(getToken(), firstName, lastName, middleName));
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
