package ru.t1.dkandakov.tm.service.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.dkandakov.tm.api.repository.model.IProjectRepository;
import ru.t1.dkandakov.tm.api.service.model.IProjectService;
import ru.t1.dkandakov.tm.enumerated.ProjectSort;
import ru.t1.dkandakov.tm.enumerated.Status;
import ru.t1.dkandakov.tm.exception.entity.EntityNotFoundException;
import ru.t1.dkandakov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dkandakov.tm.exception.entity.StatusEmptyException;
import ru.t1.dkandakov.tm.exception.field.*;
import ru.t1.dkandakov.tm.model.Project;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;

@Service
@AllArgsConstructor
public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    // public ProjectService(@NotNull final IConnectionService connectionService) {
    ///   super(connectionService);
    ///  }

    @NotNull
    @Override
    public IProjectRepository getRepository(@NotNull final EntityManager entityManager) {
        return context.getBean(IProjectRepository.class);
    }

    @NotNull
    @Override
    public List<Project> findAll(
            @Nullable final String userId,
            @Nullable final ProjectSort sort
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository repository = getRepository(entityManager);
            return repository.findAll(userId, sort);
        } catch (@NotNull final NoResultException e) {
            return null;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();

        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setStatus(Status.NOT_STARTED);
        add(userId, project);
        return project;
    }

    @NotNull
    private Project update(@Nullable final Project project) {
        if (project == null) throw new ProjectNotFoundException();

        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IProjectRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();

        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);

        return update(project);
    }

    @NotNull
    @Override
    public Project updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();

        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new EntityNotFoundException();
        project.setName(name);
        project.setDescription(description);

        return update(project);
    }

    @NotNull
    @Override
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (status == null) throw new StatusEmptyException();

        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new EntityNotFoundException();
        project.setStatus(status);

        return update(project);
    }

    @NotNull
    @Override
    public Project changeProjectStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();

        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new EntityNotFoundException();
        project.setStatus(status);

        return update(project);
    }

    @Override
    public void removeAll() {
        @Nullable final List<Project> projects = findAll();
        if (projects == null) return;
        for (final Project project : projects) {
            removeOne(project);
        }
    }

    @Override
    public void removeAll(final String userId) {
        @Nullable final List<Project> projects = findAll(userId);
        if (projects == null) return;
        for (final Project project : projects) {
            removeOne(project);
        }
    }

}
