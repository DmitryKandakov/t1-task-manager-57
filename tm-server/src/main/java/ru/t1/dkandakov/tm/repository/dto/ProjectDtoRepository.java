package ru.t1.dkandakov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.dkandakov.tm.api.repository.dto.IProjectRepositoryDTO;
import ru.t1.dkandakov.tm.dto.model.ProjectDTO;
import ru.t1.dkandakov.tm.enumerated.ProjectSort;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public final class ProjectDtoRepository extends AbstractUserOwnerDtoRepository<ProjectDTO> implements IProjectRepositoryDTO {

    @NotNull
    public ProjectDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll() {
        @NotNull final String jpql = "SELECT m FROM ProjectDTO m";
        return entityManager.createQuery(jpql, ProjectDTO.class).getResultList();
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM ProjectDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@NotNull final String userId, @NotNull final ProjectSort sort) {
        @NotNull final String jpql = "SELECT m FROM ProjectDTO m WHERE m.userId = :userId ORDER BY " + sort.getColumnName();
        return entityManager.createQuery(jpql, ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@NotNull final String id) {
        return entityManager.find(ProjectDTO.class, id);
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM ProjectDTO m WHERE m.id = :id AND m.userId = :userId";
        return entityManager.createQuery(jpql, ProjectDTO.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final ProjectDTO project) {
        @NotNull final String jpql = "DELETE FROM ProjectDTO m WHERE m.id = :projectId AND m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("projectId", project.getId())
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        Optional<ProjectDTO> model = Optional.ofNullable(findOneById(id));
        model.ifPresent(this::remove);
    }

    @Override
    public void removeOneById(@NotNull final String userId, @NotNull final String id) {
        Optional<ProjectDTO> model = Optional.ofNullable(findOneById(userId, id));
        model.ifPresent(this::remove);
    }

    @Override
    public void removeAll() {
        @NotNull final String jpql = "DELETE FROM ProjectDTO";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        @NotNull final String jpql = "DELETE FROM ProjectDTO m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public long getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM ProjectDTO m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult();
    }

    @Override
    public long getSize(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT COUNT(m) FROM ProjectDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, Long.class)
                .setParameter("userId", userId)
                .getSingleResult();
    }

}
