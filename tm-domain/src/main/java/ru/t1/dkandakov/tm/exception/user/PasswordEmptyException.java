package ru.t1.dkandakov.tm.exception.user;

public class PasswordEmptyException extends AbstractUserException {

    public PasswordEmptyException() {
        super("Error! Password is empty...");
    }

}
