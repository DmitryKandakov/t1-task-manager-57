package ru.t1.dkandakov.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.dto.request.AbstractIdRequest;
import ru.t1.dkandakov.tm.enumerated.Status;


@Getter
@Setter
@NoArgsConstructor
public final class TaskCompleteByIdRequest extends AbstractIdRequest {

    public TaskCompleteByIdRequest(
            @Nullable final String token,
            @Nullable final String id,
            @Nullable Status status
    ) {
        super(token, id);
    }

}