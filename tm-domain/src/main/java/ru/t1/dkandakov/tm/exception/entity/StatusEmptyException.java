package ru.t1.dkandakov.tm.exception.entity;

public final class StatusEmptyException extends AbstractEntityException {

    public StatusEmptyException() {
        super("Error! Status is empty...");
    }

}
